# README #

This is a Sample App to Highlight Capabilities of Fetching Data from URL, asynchronous loading of table images and Sorting of Data according to keys

### How do I Use? ###

* On starting the Application, Movie data will be fetched from URL
* Fetched  Movie data is then Sorted in Alphabetical order as per Movie Title and displayed in Table Format
* Thumbnails for the Movie names are fetched using respective URLs
* On pressing "Re-Sort" button, Ratings for each movie is fetched from the Rotten Tomatoes, using Rotten Tomato API Key and then the Movie names are sorted in the Ascending Order as per Audience Rating.