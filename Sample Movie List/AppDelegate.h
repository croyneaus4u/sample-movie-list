//
//  AppDelegate.h
//  Sample Movie List
//
//  Created by Luv Singh on 22/01/15.
//  Copyright (c) 2015 Luv Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

