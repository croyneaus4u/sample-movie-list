//
//  Movie.h
//  SampleApp
//
//  Created by Luv Singh on 22/01/15.
//  Copyright (c) 2015 Test. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Movie : NSObject <NSCopying>

@property (nonatomic, strong) NSString *movieTitle;
@property (nonatomic, strong) NSString *movieDescription;
@property (nonatomic, strong) NSURL *rottenTomatoesURL;
@property (nonatomic, strong) NSNumber *rating;
@property (nonatomic, strong) NSURL *thumbNailURL;
@property (nonatomic, strong) UIImage *thumbnailImage;

@end
