//
//  Movie.m
//  SampleApp
//
//  Created by Luv Singh on 22/01/15.
//  Copyright (c) 2015 Test. All rights reserved.
//

#import "Movie.h"

@implementation Movie

- (id)copyWithZone:(NSZone *)zone {
    id movieCopy = [[[self class] alloc] init];
    
    if (movieCopy) {
        [movieCopy setMovieTitle:self.movieTitle];
        [movieCopy setMovieDescription:self.movieDescription];
        [movieCopy setRottenTomatoesURL:self.rottenTomatoesURL];
        [movieCopy setRating:self.rating];
        [movieCopy setThumbNailURL:self.thumbNailURL];
        [movieCopy setThumbnailImage:self.thumbnailImage];
    }
    
    return movieCopy;
}

@end


