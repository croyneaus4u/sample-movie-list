//
//  TestXMLParser.h
//  SampleApp
//
//  Created by Luv Singh on 22/01/15.
//  Copyright (c) 2015 Test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Movie.h"

@protocol TestXMLParserDelegate <NSObject>

- (void)XMLParserDidFinishParsing;

@end

@interface TestXMLParser : NSObject <NSXMLParserDelegate>

@property (nonatomic, strong) NSMutableArray *moviesArray;
@property (nonatomic, weak) id <TestXMLParserDelegate> delegate;

- (id)initWithURLString:(NSString*)urlString;
- (void)startParse;
- (void)sortAlphabetically;
- (void)sortNumerically;

@end
