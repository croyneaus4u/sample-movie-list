//
//  TestXMLParser.m
//  SampleApp
//
//  Created by Luv Singh on 22/01/15.
//  Copyright (c) 2015 Test. All rights reserved.
//


const NSString *ROTTEN_TOMATOES_API_KEY = @"x26wuftnkdv422yqudubst87";

#import "TestXMLParser.h"

@interface TestXMLParser () {
    NSXMLParser *parser;
    Movie *movieObject;
    NSString *element;
}

@end

@implementation TestXMLParser

- (id)initWithURLString:(NSString*)urlString {
    self = [[TestXMLParser alloc] init];
    if (self) {
        _moviesArray = [[NSMutableArray alloc] init];
        NSURL *url = [NSURL URLWithString:urlString];
        parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
        [parser setDelegate:self];
        [parser setShouldResolveExternalEntities:NO];
    }
    
    return self;
}

- (void)startParse {
    [parser performSelectorInBackground:@selector(parse) withObject:nil];
}

#pragma mark - parser delegates

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    element = elementName;
    
    if ([element isEqualToString:@"item"]) {
        movieObject = [[Movie alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"item"]) {
        [self.moviesArray addObject:[movieObject copy]];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if ([element isEqualToString:@"title"]) {
        movieObject.movieTitle = string;
        [self setUpRottenTomatoesURL:string];
    } else if ([element isEqualToString:@"description"]) {
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        movieObject.movieDescription = string;
        [self setupThumbnailURL:string];
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    
    [self sortAlphabetically];
    
    [self performSelectorOnMainThread:@selector(finishedParsing) withObject:nil waitUntilDone:NO];
}

- (void)sortAlphabetically {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"movieTitle" ascending:YES selector:@selector(caseInsensitiveCompare:)];
    [self.moviesArray sortUsingDescriptors:@[sortDescriptor]];
}

- (void)sortNumerically {
    [self.moviesArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        if ([[obj1 rating] intValue] < [[obj2 rating] intValue]) {
            return NSOrderedAscending;
        } else
            return NSOrderedDescending;
    }];
}

- (void)finishedParsing {
    if ([self.delegate respondsToSelector:@selector(XMLParserDidFinishParsing)]) {
        [self.delegate XMLParserDidFinishParsing];
    }
}

#pragma mark - movie methods

- (void)setUpRottenTomatoesURL:(NSString*)movieTitle {
    NSString *urlString = [NSString stringWithFormat:@"http://api.rottentomatoes.com/api/public/v1.0/movies.json?apikey=%@&q=%@",ROTTEN_TOMATOES_API_KEY,movieTitle];
    urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    movieObject.rottenTomatoesURL = [NSURL URLWithString:urlString];
}

- (void)setupThumbnailURL:(NSString*)description {
    NSString *regExString = @"((www\\.|(http|https|ftp|news|file)+\\:\\/\\/)[&#95;.a-z0-9-_]+\\.[a-z0-9_\\/&#95;:@=.+?,##%&~-]*[^.|\'|\\# |!|\(|?|,| |>|<|;|\\)])";
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regExString options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSArray *array = [regex matchesInString:description options:NSMatchingReportCompletion range:NSMakeRange(0, description.length)];
    for (NSTextCheckingResult *result in array) {
        NSRange range = result.range;
        NSString *thumbnailURLString = [description substringWithRange:range];
        if ([[thumbnailURLString lowercaseString] containsString:@".jpg"]) {
            if ([thumbnailURLString hasSuffix:@"\""]) {
                thumbnailURLString = [thumbnailURLString stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            }
            movieObject.thumbNailURL = [NSURL URLWithString:thumbnailURLString];
            break;
        }
    }
}

@end
