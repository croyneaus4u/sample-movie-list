//
//  ViewController.m
//  Sample Movie List
//
//  Created by Luv Singh on 22/01/15.
//  Copyright (c) 2015 Luv Singh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () {
    
    TestXMLParser *_testXMLParser;
    dispatch_queue_t _queue;
    dispatch_queue_t _sortQueue;
    
    BOOL _sortingGoingOn;
    BOOL _isNumericallySorted;
    BOOL _sortingComplete;
    UIActivityIndicatorView *_activityView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _queue = dispatch_queue_create("com.test.images", NULL);
    _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityView.hidesWhenStopped = YES;
    
    NSString *urlString = @"http://www.fandango.com/rss/newmovies.rss";
    _testXMLParser = [[TestXMLParser alloc] initWithURLString:urlString];
    _testXMLParser.delegate = self;
    [_testXMLParser startParse];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Re-Sort" style:UIBarButtonItemStyleDone target:self action:@selector(sortAlternatively:)];
    self.navigationItem.rightBarButtonItem = barButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sortAlternatively:(id)sender {
    if (!_isNumericallySorted) {
        if (_sortingComplete) {
            [self sortMoviesArrayAsPerSortCondition];
            return;
        }
        _sortingGoingOn = YES;
        if (!_sortQueue) {
            _sortQueue = dispatch_queue_create("com.test.ratings", NULL);
        }
        
        __block int count = 0;
        _activityView.center = self.view.center;
        [self.navigationController.view addSubview:_activityView];
        [_activityView startAnimating];
        for (Movie *movieObject in _testXMLParser.moviesArray) {
            dispatch_async(_sortQueue, ^{
                NSData *movieData = [[NSData alloc] initWithContentsOfURL:movieObject.rottenTomatoesURL];
                NSJSONSerialization *jsonObject = [NSJSONSerialization JSONObjectWithData:movieData options:NSJSONReadingAllowFragments error:NULL];
                movieObject.rating = [[[jsonObject valueForKey:@"movies"] objectAtIndex:0] valueForKeyPath:@"ratings.audience_score"];
                count ++;
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (count == _testXMLParser.moviesArray.count) {
                        _sortingComplete = YES;
                        [_activityView stopAnimating];
                        [_activityView removeFromSuperview];
                        [self sortMoviesArrayAsPerSortCondition];
                    }
                });
            });
        }
    } else {
        [self sortMoviesArrayAsPerSortCondition];
    }
    
}

- (void)sortMoviesArrayAsPerSortCondition {
    if (!_isNumericallySorted) {
        _sortingGoingOn = NO;
        [_testXMLParser sortNumerically];
        _isNumericallySorted = YES;
    } else {
        [_testXMLParser sortAlphabetically];
        _isNumericallySorted = NO;
    }
    
    [self.tableView reloadData];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _testXMLParser.moviesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    Movie *movieObject = [_testXMLParser.moviesArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [movieObject movieTitle];

    [self loadThumbnailImageforCell:cell forMovieObject:movieObject];
    
    return cell;
}

- (void)loadThumbnailImageforCell:(UITableViewCell*)cell forMovieObject:(Movie*)movieObject {
    
    cell.imageView.image = nil;
    __block __weak __typeof__ (self) weakSelf = self;
    if (!movieObject.thumbnailImage) {
        dispatch_async(_queue, ^{
            NSData *data = [[NSData alloc] initWithContentsOfURL:movieObject.thumbNailURL];
            UIImage *image = [UIImage imageWithData:data scale:0.5];
            movieObject.thumbnailImage = image;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                ViewController *strongSelf = weakSelf;
                [strongSelf loadThumbnailImageforCell:cell forMovieObject:movieObject];
            });
        });
    } else {
        cell.imageView.image = movieObject.thumbnailImage;
        [cell setNeedsLayout];
    }
}

#pragma mark - TestXMLParserDelegate method

- (void)XMLParserDidFinishParsing {
    // brought control to main thread here
}


@end
