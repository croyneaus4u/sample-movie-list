//
//  main.m
//  Sample Movie List
//
//  Created by Luv Singh on 22/01/15.
//  Copyright (c) 2015 Luv Singh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
